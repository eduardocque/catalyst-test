<?php
/**
 * Created by PhpStorm.
 * User: Carlos Rodriguez
 * Date: 30/07/2016
 * Time: 08:01 PM
 */

defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));

$ar =  range(1,100,1);

fwrite(\STDOUT,"Before of the process:\n");
fwrite(\STDOUT,implode(', ',$ar) . "\n");
for($i = 0; $i < count($ar); $i++)
{
    if($ar[$i]%3 == 0 && $ar[$i]%5 == 0)
        $ar[$i] = 'foobar';
    elseif($ar[$i]%5 == 0)
        $ar[$i] = 'bar';
    elseif($ar[$i]%3 == 0)
        $ar[$i] = 'foo';
}
fwrite(\STDOUT,"After of the process:\n");
fwrite(\STDOUT,implode(', ',$ar) . "\n");