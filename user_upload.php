<?php
/**
 * Created by PhpStorm.
 * User: Carlos Rodriguez
 * Date: 30/07/2016
 * Time: 03:30 PM
 */

//defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));
defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'w'));
//defined('STDERR') or define('STDERR', fopen('php://stderr', 'w'));

class dbManager{
    public $dbhost = 'localhost';
    public $dbname = 'test';
    public $dbuser = 'root';
    public $dbpass = '';
    public $conn = null;

    function __construct($host,$name,$user,$pass){
        if(!isset($host))
            $host = $this->dbhost;
        if(!isset($name))
            $name = $this->dbname;
        if(!isset($user))
            $user = $this->dbuser;
        if(!isset($pass))
            $pass = $this->dbpass;
        $this->conn = @new mysqli($host,$user,$pass,$name);
        if ($this->conn->connect_error) {
            fwrite(\STDOUT,"Connection failed: " . $this->conn->connect_error);
            exit(0);
        }
    }

    function __destruct() {
        $this->conn->close();
        $this->conn = null;
    }

    const TABLE_USER = 'user';

    //Functions - Funciones

    function createTable($table)
    {
        $sql = null;
        switch($table)
        {
            case self::TABLE_USER:
                $sql = "CREATE TABLE $table (`id` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(255) NOT NULL,`surname` VARCHAR(255) NOT NULL,`email` VARCHAR(255) NOT NULL,PRIMARY KEY (`id`))";
        }
        if(isset($sql))
        {
            if ($this->conn->query($sql))
                return "Table created";
            else
                return $this->conn->error;
        }
        else
            return "Option Incorrect";
    }

    function insertUser($name,$surname,$email)
    {
        $sql = "INSERT INTO " . $this::TABLE_USER . " (name,surname,email) VALUES ('$name','$surname','$email')";
        if($this->conn->query($sql))
            return $this->conn->insert_id;
        else
            return 0;
    }
}

function arguments($argv) {
    $_ARG = [];
    foreach ($argv as $arg) {
        if (preg_match('/--([^=]+)=(.*)/',$arg,$reg))
            $_ARG[$reg[1]] = $reg[2];
        elseif(preg_match('/--([^=]+)\[(.*)\]/',$arg,$reg))
            $_ARG[$reg[1]] = $reg[2];
        elseif(preg_match('/--([^=]+)/',$arg,$reg))
            $_ARG[$reg[1]] = 'true';
        elseif(preg_match('/-([^=]+)=(.*)/',$arg,$reg))
            $_ARG[$reg[1]] = $reg[2];
        elseif(preg_match('/-([a-zA-Z0-9])/',$arg,$reg))
            $_ARG[$reg[1]] = 'true';
    }
    return $_ARG;
}

function file_reader($args)
{
    $dataFile = null;
    if(file_exists(__DIR__ . '/'.$args['file']))
    {
        $fileName = __DIR__ . '/'.$args['file'];
        $f = fopen($fileName,'r');
        try
        {
            while (! feof($f))
            {
                $line = fgetcsv($f);
                if(isset($line) && !empty($line))
                    $dataFile []= $line;
            }
        }
        catch(Exception $e)
        {
            fwrite(\STDOUT,"Error during the parse of the .csv file \n");
            $dataFile = null;
        }
        fclose($f);
        array_shift($dataFile);  //column names of the csv file
        return $dataFile;
    }
    else
        fwrite(\STDOUT,"file not found \n");
    return $dataFile;
}

function insertUsers($db,$data,$ignoreEntry)
{
    try
    {
        foreach($data as $user)
        {
            print_r($user);
            if(filter_var($user[2],FILTER_VALIDATE_EMAIL))
            {
                if(!$ignoreEntry)
                {
                    if($db->insertUser($db->conn->escape_string($user[0]),$db->conn->escape_string($user[1]),$db->conn->escape_string($user[2])) > 0)
                        fwrite(\STDOUT,"Name: " . $user[0] . " Added \n");
                    else
                        fwrite(\STDOUT,"Name: " . $user[0] .  " Error Adding this user \n");
                    print_r($db->conn->error);
                }
                else
                    fwrite(\STDOUT,"Name: " . $user[0] .  " Insertion ignored \n");
            }
            else
                fwrite(\STDOUT,"Name: " . $user[0] . " have a invalid email \n");
        }
        fwrite(\STDOUT,"Users saved in the database \n");
    }
    catch(Exception $e)
    {
        fwrite(\STDOUT,"ERROR Adding the users in the database \n");
    }
}

//CONSOLE
$data = arguments($argv);
$h = null;
$u = null;
$p = null;
$n = null;
if(array_key_exists('h',$data))
    $h = $data['h'];
if(array_key_exists('n',$data))
    $n = $data['n'];
if(array_key_exists('u',$data))
    $u = $data['u'];
if(array_key_exists('p',$data))
    $p = $data['p'];
$db = new dbManager($h,$n,$u,$p);

if(array_key_exists('create_table',$data))
{
    fwrite(\STDOUT,$db->createTable(dbManager::TABLE_USER) . "\n");
    exit(0);
}

if(array_key_exists('file',$data))
{
    $fileData = file_reader($data);
    if(isset($fileData))
        insertUsers($db,$fileData,array_key_exists('dry_run',$data));
}

if(array_key_exists('help',$data))
{
    fwrite(\STDOUT,"Methods Allowed:\n==========\n--file[filename] or --file=filename\n--dry_run\n--create_table\n-u\n-p\n-h\n==========");
}
exit(0);